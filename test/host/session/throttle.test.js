const throttle = require('../../../host/session/throttle')

const delay = ms => new Promise(r => setTimeout(r, ms))

test("fires leading execution immediately", async () => {
  const spy = jest.fn((x, y) => x + y)
  const t = throttle(spy)
  const result = t(1, 2)
  expect(spy.mock.calls).toEqual([[1, 2]])
  expect(await result).toBe(3)
})

test("fires second execution after timeout", async () => {
  const spy = jest.fn((x, y) => x + y)
  const t = throttle(spy, { timeout: 10 })
  const r1 = t(1, 2)
  const r2 = t(3, 4)
  expect(await r1).toBe(3)
  await delay()
  expect(spy.mock.calls).toEqual([[1, 2]])
  await delay(10)
  expect(spy.mock.calls).toEqual([[1, 2], [3, 4]])
  expect(await r2).toBe(7)
})

test("fires only once per timeout, using the most recent arguments", async () => {
  const spy = jest.fn((x, y) => x + y)
  const t = throttle(spy, { timeout: 10 })
  const r1 = t(1, 2)
  const r2 = t(3, 4)
  const r3 = t(5, 6)
  await delay(10)
  expect(spy.mock.calls).toEqual([[1, 2], [5, 6]])
  expect(await r1).toBe(3)
  expect(await r2).toBe(11)
  expect(await r3).toBe(11)
  await delay(10)
  expect(spy.mock.calls).toEqual([[1, 2], [5, 6]])
})

test("timeout has a tail", async () => {
  const spy = jest.fn()
  const t = throttle(spy, { timeout: 10 })
  t(1)
  t(2)
  t(3)
  expect(spy.mock.calls).toEqual([[1]])
  await delay(10)
  expect(spy.mock.calls).toEqual([[1], [3]])
  t(4)
  t(5)
  expect(spy.mock.calls).toEqual([[1], [3]])
  await delay(10)
  expect(spy.mock.calls).toEqual([[1], [3], [5]])
  t(6)
  t(7)
  expect(spy.mock.calls).toEqual([[1], [3], [5]])
  await delay(15)
  expect(spy.mock.calls).toEqual([[1], [3], [5], [7]])
  await delay(10)
  expect(spy.mock.calls).toEqual([[1], [3], [5], [7]])
})

test("timeout resets", async () => {
  const spy = jest.fn()
  const t = throttle(spy, { timeout: 10 })
  t(1)
  await delay(20)
  t(2)
  expect(spy.mock.calls).toEqual([[1], [2]])
})

test("exceptions are flowed", async () => {
  const t = throttle(x => { throw new Error(x) }, { timeout: 0 })
  const r1 = t(1)
  const r2 = t(2)
  const r3 = t(3)
  await expect(r1).rejects.toEqual(new Error(1))
  await expect(r2).rejects.toEqual(new Error(3))
  await expect(r3).rejects.toEqual(new Error(3))
})

