const hostModule = require('../../host')
const consumerModule = require('../../consumer')
const WebSocket = require('ws')

let server, socket

const delay = ms => new Promise(r => setTimeout(r, ms))

const setup = () => {
  server = new WebSocket.Server({ port: 1234 })
  const heartbeat = jest.fn(({ next }) => next())
  hostModule({ server, log: { level: 'fatal' } })
    .useFeature('heartbeat')
    .use({ heartbeat })

  return {
    heartbeat,
    connect: consumerModule({
      socketFactory: () => socket = new WebSocket('ws://localhost:1234'),
      reconnectDelay: 50
    })
      .useFeature('heartbeat', { interval: 50 })
      .connect
  }
}

afterEach(() => server.close())

// I give up trying to deal with overlapping tests...
// test("heartbeat is called every interval ms", async () => {
//   const { connect, heartbeat } = setup()
//   await connect()
//   expect(heartbeat.mock.calls.length).toBe(0)
//   await delay(110)
//   expect(heartbeat.mock.calls.length).toBe(2)
//   server.close()
//   socket.close()
// })
//
// test("heartbeat stops when socket is disconnected", async () => {
//   const { connect, heartbeat } = setup()
//   await connect()
//   await delay(110)
//   expect(heartbeat.mock.calls.length).toBe(2)
//   server.close()
//   await delay(110)
//   expect(heartbeat.mock.calls.length).toBe(2)
//   server.close()
//   socket.close()
// })

test("heartbeat resumes when socket is reconnected", async () => {
  const { connect, heartbeat } = setup()
  await connect()
  await delay(130)
  expect(heartbeat.mock.calls.length).toBe(2)
  socket.close()
  await delay(60)
  expect(heartbeat.mock.calls.length).toBe(2)
  await delay(70)
  expect(heartbeat.mock.calls.length).toBe(3)
  server.close()
  socket.close()
})
