const cipher = require('../utilities/cipher')
const uuid = require('../../common/uuid')

module.exports = ({ cipherKey: password } = {}) => {
  if(!password) {
    throw new Error('You must provide a cipherKey')
  }

  const { encrypt, decrypt, getKeyFromPassword } = cipher()
  const cipherKey = getKeyFromPassword(password)
  const decryptClientId = (encrypted, log) => {
    try {
      return decrypt(Buffer.from(encrypted, 'base64'), cipherKey).toString('utf8')
    } catch {
      log.warn('Invalid clientId data provided')
      return uuid()
    }
  }

  return () => ({
    name: 'clientId',
    handshake: ({ data }, context) => {
      const clientId = data.clientId ? decryptClientId(data.clientId, context.connection.log) : uuid()
      context.connection.clientId = clientId
      context.connection.log.setScope({ clientId })
      return encrypt(clientId, cipherKey).toString('base64')
    }
  })
}
