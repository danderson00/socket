module.exports = {
  sources: {
    HOST: 1,
    CONSUMER: 2
  },
  errorCodes: {
    HANDSHAKE_FAILED: 128
  }
}